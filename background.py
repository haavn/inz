import cv2
import numpy as np
from time import localtime, strftime, sleep


STREAM_ADDRESS = 'http://hoktastream1.webcamera.pl/wentzl_cam_be5c6c/wentzl_cam_be5c6c.stream/playlist.m3u8'
HEATMAP_SAVE_BLACKLIST = 99
HEATMAP_SAVE_INTERVALS = (1, 2, 5, 10, 15, 20, 30, 60)
INTERVAL = 60
ARRAY_SIZE = 16
AREA_OF_INTEREST = (500, 1215)


def setup(interval, array_size):
    assert interval in HEATMAP_SAVE_INTERVALS, "Wrong interval, should be " + str(HEATMAP_SAVE_INTERVALS)
    cam = open_stream(STREAM_ADDRESS)
    fgbg = cv2.createBackgroundSubtractorMOG2(history=300, varThreshold=300, detectShadows=True)
    assert array_size in (8, 16), "Wrong array size, should be 8 or 16"
    heatmap_array = np.zeros(AREA_OF_INTEREST, np.uint8 if ARRAY_SIZE == 8 else np.uint16)

    return cam, fgbg, heatmap_array


def open_stream(stream_address):
    while True:
        cam = cv2.VideoCapture(stream_address)
        if cam.isOpened():
            print("Succesfully opened stream")
            break
        print("error opening stream, retrying in 10 seconds")
        sleep(10)
    return cam


cam, fgbg, heatmap_array = setup(INTERVAL, ARRAY_SIZE)

#fullbody_cascade = cv2.CascadeClassifier('haarcascade_fullbody.xml')
#fgbg = cv2.createBackgroundSubtractorMOG2(history=300, varThreshold=300, detectShadows=DETECT_SHADOWS)

#cam = cv2.VideoCapture('videos/l_1205056_86304701_14014.ts')
#cam = cv2.VideoCapture('http://hoktastream1.webcamera.pl/wentzl_cam_be5c6c/wentzl_cam_be5c6c.stream/playlist.m3u8')

#heatmap_array = np.zeros((450, 1315), np.uint8)

kernel_vertical = np.ones((2,1),np.uint8)
kernel_horizontal= np.ones((1,2),np.uint8)
kernel_square = cv2.getStructuringElement(cv2.MORPH_RECT, (2,2))
kernel2 = cv2.getStructuringElement(cv2.MORPH_CROSS,(3,3))

while True:
    ret, img1 = cam.read()

    if not ret or img1 is None:
        print("End of stream, retrying...")
        cam = open_stream(STREAM_ADDRESS)
        ret, img1 = cam.read()
        if img1 is None:
            ret = False
        #break

    img1 = img1[1080-AREA_OF_INTEREST[0]:, 305:305+AREA_OF_INTEREST[1]]
    #cv2.imwrite("image.png", img1)
    img1_gr = cv2.GaussianBlur(cv2.cvtColor(img1, cv2.COLOR_RGB2GRAY), (5, 5), 0)
    bg = fgbg.apply(img1_gr)
    r, bg_thr = cv2.threshold(bg, 200, 255, cv2.THRESH_BINARY)

    #erosion = cv2.erode(bg_thr, kernel)
    #opening = cv2.morphologyEx(bg_thr, cv2.MORPH_OPEN, kernel_square, iterations=1)

    dilate = cv2.dilate(bg_thr, None, iterations=2)

    _, contours, hierarchy = cv2.findContours(dilate, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    for c in contours:
        if cv2.contourArea(c) < 4:
            continue

        x, y, w, h = cv2.boundingRect(c)
        if h > w:
            heatmap_array[y+h-1][x+int(w/2)] += 1
            cv2.rectangle(img1, (x, y), (x + w, y + h), (0, 255, 0), 2)

    cv2.imshow('ORIGINAL', img1)

   # cv2.imshow('opening', opening)
   # cv2.imshow('erosion', erosion)
   # cv2.imshow('dilation', dilate)
   # cv2.imshow('fgbg', bg)
   # cv2.imshow('SUM', cv2.add(img1, cv2.cvtColor(opening, cv2.COLOR_GRAY2BGR)))
   # cv2.imshow('bg_thr', bg_thr)
   # cv2.imshow('keypoints', im_with_keypoints)

    cv2.imshow('heatmap', heatmap_array)

    current_time = localtime()
    if current_time.tm_hour != HEATMAP_SAVE_BLACKLIST and current_time.tm_min == 0:
        #if current_time.tm_min % INTERVAL == 0:
        filename = strftime("%y%m%d_%H%M", current_time) + ".npy"
        # TODO: add division of points far from camera
        np.save("heatmap_data/" + filename, heatmap_array)
        heatmap_array = np.zeros(AREA_OF_INTEREST, np.uint8 if ARRAY_SIZE == 8 else np.uint16)
        HEATMAP_SAVE_BLACKLIST = current_time.tm_hour
        print "Saved file:", filename

    if cv2.waitKey(30) == 27:
        break

cam.release()
cv2.destroyAllWindows()



