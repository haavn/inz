import cv2
import numpy as np
import time
import Queue

q = Queue.Queue(10)

#cam = cv2.VideoCapture('videos/l_1205056_86274235_14009.ts')
cam = cv2.VideoCapture('http://hoktastream1.webcamera.pl/wentzl_cam_be5c6c/wentzl_cam_be5c6c.stream/playlist.m3u8')

kernel = np.ones((3,3),np.uint8)

while True:
    f1, img1 = cam.read()
    img1 = img1[640:1079, 150:1500]
    img1 = cv2.cvtColor(img1, cv2.COLOR_RGB2GRAY)

    if q.full():
        img2 = q.get()
        img_diff = cv2.absdiff(img2, img1)
        ret, img_thresh = cv2.threshold(img_diff, 40, 255, cv2.THRESH_BINARY)

        #img3 = cv2.add(img2, img_diff)

        opening = cv2.morphologyEx(img_thresh, cv2.MORPH_OPEN, kernel)

        #print opening

        cv2.imshow('DIFF', img_diff)
        cv2.imshow('ORIGINAL', img1)
        cv2.imshow('THRESHOLD', img_thresh)
        cv2.imshow('opening', opening)

    else:
        q.put(img1)

    if cv2.waitKey(30) == 27:
        break

cam.release()
cv2.destroyAllWindows()