import cv2
import numpy as np
import os
import matplotlib.pyplot as plt
import seaborn as sns


#HEATMAP_COLORMAP = cv2.COLORMAP_HOT
HEATMAP_COLORMAP = cv2.COLORMAP_JET

list_of_arrays = []
suum = np.zeros((450, 1315), np.uint16)
dir = 'heatmap_data/'
for filename in os.listdir(dir):
    if filename.endswith('.npy'):
        #list_of_arrays.append(np.load(dir+filename))
        temp = np.load(dir+filename) * 10
        #print(np.amax(temp))
        suum = np.add(suum, temp)
print(np.amax(suum))
print(type(suum[2][2]))

norm_scalar = np.amax(suum)/255.
print(norm_scalar)
heatmap = suum/norm_scalar
print(np.amax(heatmap))
print(type(heatmap[2][2]))
heatmap = heatmap.astype(np.uint8)
print(np.amax(heatmap))
print(type(heatmap[2][2]))

heatmap[heatmap < 10] = 0
heatmap = cv2.applyColorMap(heatmap, HEATMAP_COLORMAP)
#heatmap = cv2.cvtColor(heatmap, cv2.COLOR_GRAY2RGB)
#heatmap[:,:,:2] = 0
#print heatmap.shape
original = cv2.imread('image.png')
original = cv2.cvtColor(original, cv2.COLOR_RGB2GRAY)
original = cv2.cvtColor(original, cv2.COLOR_GRAY2RGB)
#print original.shape
output = original.copy()
cv2.addWeighted(heatmap, 0.7, original, 0.3, 0, output)
#sns.heatmap(heatmap)
#a = plt.imshow(heatmap, cmap='jet', interpolation='nearest')
#plt.show()

cv2.imshow('heatmap', output)
cv2.waitKey(0)
cv2.destroyAllWindows()